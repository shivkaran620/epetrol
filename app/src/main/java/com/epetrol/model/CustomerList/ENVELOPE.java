package com.epetrol.model.CustomerList;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.net.URL;
import java.util.List;

@Root(name="ENVELOPE")
public class ENVELOPE {

   /* @Text(required=false)
    String textValue;*/

    @Element(name="BODY", required=false)
    BODY bODY;

   /* public String getTextValue() {return this.textValue;}
    public void setTextValue(String value) {this.textValue = value;}*/

    public BODY getBODY() {return this.bODY;}
    public void setBODY(BODY value) {this.bODY = value;}

    public static class CMPINFO {

        @Element(name="LEDGER", required=false)
        String lEDGER;

        public String getLEDGER() {return this.lEDGER;}
        public void setLEDGER(String value) {this.lEDGER = value;}

    }

    public static class DESC {

        @Element(name="CMPINFO", required=false)
        CMPINFO cMPINFO;

        public CMPINFO getCMPINFO() {return this.cMPINFO;}
        public void setCMPINFO(CMPINFO value) {this.cMPINFO = value;}

    }

    public static class LANGUAGENAME_LIST {

        @Element(name="NAME", required=false)
        String nAME;

        public String getNAME() {return this.nAME;}
        public void setNAME(String value) {this.nAME = value;}



    }

    public static class DATA {

        @ElementList(name="COLLECTION", required=false)
        List<LEDGER> cOLLECTION;

        public List<LEDGER> getCOLLECTION() {return this.cOLLECTION;}
        public void setCOLLECTION(List<LEDGER> value) {this.cOLLECTION = value;}

    }

    public static class LEDGER {

        @Element(name="GSTREGISTRATIONTYPE", required=false)
        String gSTREGISTRATIONTYPE;
        @Element(name = "ID", required = false)
        String ID;

        @Element(name="VATAPPLICABLE", required=false)
        String vATAPPLICABLE;

        @Element(name="LEDSTATENAME", required=false)
        String lEDSTATENAME;

        @Element(name="GUID", required=false)
        String gUID;

        @Element(name="PARTYGSTIN", required=false)
        String pARTYGSTIN;

        @Element(name="LEDGERCONTACT", required=false)
        String lEDGERCONTACT;

        @Element(name="LEDGERPHONE", required=false)
        String lEDGERPHONE;

        @Element(name="TAXTYPE", required=false)
        String tAXTYPE;

        @Element(name="LANGUAGENAME.LIST", required=false)
        LANGUAGENAME_LIST lANGUAGENAME_LIST;

        @ElementList(name="ADDRESS.LIST", required=false)
        List<String> aDDRESS_LIST;

        @Element(name="ALTERID", required=false)
        String aLTERID;

        @Element(name="PARENT", required=false)
        String pARENT;

        @Element(name="_PRIMARYGROUP", required=false)
        String PRIMARYGROUP;

        @Element(name="COUNTRYNAME", required=false)
        String cOUNTRYNAME;

        @Element(name="GSTAPPLICABLE", required=false)
        String gSTAPPLICABLE;

        public String getGSTREGISTRATIONTYPE() {return this.gSTREGISTRATIONTYPE;}
        public void setGSTREGISTRATIONTYPE(String value) {this.gSTREGISTRATIONTYPE = value;}

        public String getVATAPPLICABLE() {return this.vATAPPLICABLE;}
        public void setVATAPPLICABLE(String value) {this.vATAPPLICABLE = value;}

        public String getLEDSTATENAME() {return this.lEDSTATENAME;}
        public void setLEDSTATENAME(String value) {this.lEDSTATENAME = value;}

        public String getGUID() {return this.gUID;}
        public void setGUID(String value) {this.gUID = value;}

        public String getPARTYGSTIN() {return this.pARTYGSTIN;}
        public void setPARTYGSTIN(String value) {this.pARTYGSTIN = value;}

        public String getLEDGERCONTACT() {return this.lEDGERCONTACT;}
        public void setLEDGERCONTACT(String value) {this.lEDGERCONTACT = value;}

        public String getLEDGERPHONE() {return this.lEDGERPHONE;}
        public void setLEDGERPHONE(String value) {this.lEDGERPHONE = value;}

        public String getTAXTYPE() {return this.tAXTYPE;}
        public void setTAXTYPE(String value) {this.tAXTYPE = value;}

        public LANGUAGENAME_LIST getLANGUAGENAME_LIST() {return this.lANGUAGENAME_LIST;}
        public void setLANGUAGENAME_LIST(LANGUAGENAME_LIST value) {this.lANGUAGENAME_LIST = value;}

        public List<String> getADDRESS_LIST() {return this.aDDRESS_LIST;}
        public void setADDRESS_LIST(List<String> value) {this.aDDRESS_LIST = value;}

        public String getALTERID() {return this.aLTERID;}
        public void setALTERID(String value) {this.aLTERID = value;}

        public String getPARENT() {return this.pARENT;}
        public void setPARENT(String value) {this.pARENT = value;}

        public String getPRIMARYGROUP() {return this.PRIMARYGROUP;}
        public void setPRIMARYGROUP(String value) {this.PRIMARYGROUP = value;}

        public String getCOUNTRYNAME() {return this.cOUNTRYNAME;}
        public void setCOUNTRYNAME(String value) {this.cOUNTRYNAME = value;}

        public String getGSTAPPLICABLE() {return this.gSTAPPLICABLE;}
        public void setGSTAPPLICABLE(String value) {this.gSTAPPLICABLE = value;}

        public String getID() {
            return this.ID;
        }

        public void setID(String value) {
            this.ID = value;
        }





        @Override
        public String toString() {
            return this.lANGUAGENAME_LIST.nAME; // What to display in the Spinner list.
        }

    }

    public static class BODY {

        @Element(name="DATA", required=false)
        DATA dATA;

        @Element(name="DESC", required=false)
        DESC dESC;

        public DATA getDATA() {return this.dATA;}
        public void setDATA(DATA value) {this.dATA = value;}

        public DESC getDESC() {return this.dESC;}
        public void setDESC(DESC value) {this.dESC = value;}

    }

}