package com.epetrol.model.Item;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.net.URL;
import java.util.List;

@Root(name = "ENVELOPE")
public class ENVELOPE {

   /* @Text(required=false)
    String textValue;*/

    @Element(name = "BODY", required = false)
    BODY bODY;

    /* public String getTextValue() {return this.textValue;}
     public void setTextValue(String value) {this.textValue = value;}
 */
    public BODY getBODY() {
        return this.bODY;
    }

    public void setBODY(BODY value) {
        this.bODY = value;
    }

    public static class CMPINFO {

        @Element(name = "STOCKITEM", required = false)
        String sTOCKITEM;

        public String getSTOCKITEM() {
            return this.sTOCKITEM;
        }

        public void setSTOCKITEM(String value) {
            this.sTOCKITEM = value;
        }

    }

    public static class VATDETAILSLIST {

        @Element(name = "RATEOFVAT", required = false)
        String rATEOFVAT;

        public String getRATEOFVAT() {
            return this.rATEOFVAT;
        }

        public void setRATEOFVAT(String value) {
            this.rATEOFVAT = value;
        }

    }

    public static class DESC {

        @Element(name = "CMPINFO", required = false)
        CMPINFO cMPINFO;

        public CMPINFO getCMPINFO() {
            return this.cMPINFO;
        }

        public void setCMPINFO(CMPINFO value) {
            this.cMPINFO = value;
        }

    }

    public static class DATA {

        @ElementList(name = "COLLECTION", required = false)
        List<STOCKITEM> cOLLECTION;

        public List<STOCKITEM> getCOLLECTION() {
            return this.cOLLECTION;
        }

        public void setCOLLECTION(List<STOCKITEM> value) {
            this.cOLLECTION = value;
        }

    }

    public static class LANGUAGENAMELIST {

        @Element(name = "NAME", required = false)
        String nAME;

        public String getNAME() {
            return this.nAME;
        }

        public void setNAME(String value) {
            this.nAME = value;
        }

    }

    public static class RATEDETAILSLIST {

        @Element(name = "GSTRATEDUTYHEAD", required = false)
        String gSTRATEDUTYHEAD;

        @Element(name = "GSTRATEVALUATIONTYPE", required = false)
        String gSTRATEVALUATIONTYPE;

        @Element(name = "GSTRATE", required = false)
        String gSTRATE;

        public String getGSTRATEDUTYHEAD() {
            return this.gSTRATEDUTYHEAD;
        }

        public void setGSTRATEDUTYHEAD(String value) {
            this.gSTRATEDUTYHEAD = value;
        }

        public String getGSTRATEVALUATIONTYPE() {
            return this.gSTRATEVALUATIONTYPE;
        }

        public void setGSTRATEVALUATIONTYPE(String value) {
            this.gSTRATEVALUATIONTYPE = value;
        }

        public String getGSTRATE() {
            return this.gSTRATE;
        }

        public void setGSTRATE(String value) {
            this.gSTRATE = value;
        }

    }

    public static class BODY {

        @Element(name = "DATA", required = false)
        DATA dATA;

        @Element(name = "DESC", required = false)
        DESC dESC;

        public DATA getDATA() {
            return this.dATA;
        }

        public void setDATA(DATA value) {
            this.dATA = value;
        }

        public DESC getDESC() {
            return this.dESC;
        }

        public void setDESC(DESC value) {
            this.dESC = value;
        }

    }

    public static class GSTDETAILSLIST {

        @ElementList(name = "STATEWISEDETAILS.LIST", required = false)
        List<RATEDETAILSLIST> sTATEWISEDETAILSLIST;

        @Element(name = "HSNMASTERNAME", required = false)
        String hSNMASTERNAME;

        public List<RATEDETAILSLIST> getSTATEWISEDETAILSLIST() {
            return this.sTATEWISEDETAILSLIST;
        }

        public void setSTATEWISEDETAILSLIST(List<RATEDETAILSLIST> value) {
            this.sTATEWISEDETAILSLIST = value;
        }

        public String getHSNMASTERNAME() {
            return this.hSNMASTERNAME;
        }

        public void setHSNMASTERNAME(String value) {
            this.hSNMASTERNAME = value;
        }

    }

    public static class STOCKITEM {

        @Element(name = "ID", required = false)
        String ID;

        @Element(name = "VATAPPLICABLE", required = false)
        String vATAPPLICABLE;


        @Element(name = "ALTERID", required = false)
        String aLTERID;

        @Element(name = "VATDETAILS.LIST", required = false)
        VATDETAILSLIST vATDETAILSLIST;

        @Element(name = "LANGUAGENAME.LIST", required = false)
        LANGUAGENAMELIST lANGUAGENAMELIST;

        @Element(name = "PARENT", required = false)
        String pARENT;

        @Element(name = "GUID", required = false)
        String gUID;

        @Element(name = "BASEUNITS", required = false)
        String bASEUNITS;

        @Element(name = "GSTAPPLICABLE", required = false)
        String gSTAPPLICABLE;

        @Element(name = "GSTDETAILS.LIST", required = false)
        GSTDETAILSLIST gSTDETAILSLIST;

        public String getVATAPPLICABLE() {
            return this.vATAPPLICABLE;
        }

        public void setVATAPPLICABLE(String value) {
            this.vATAPPLICABLE = value;
        }

        public String getALTERID() {
            return this.aLTERID;
        }

        public void setALTERID(String value) {
            this.aLTERID = value;
        }

        public VATDETAILSLIST getVATDETAILSLIST() {
            return this.vATDETAILSLIST;
        }

        public void setVATDETAILSLIST(VATDETAILSLIST value) {
            this.vATDETAILSLIST = value;
        }

        public LANGUAGENAMELIST getLANGUAGENAMELIST() {
            return this.lANGUAGENAMELIST;
        }

        public void setLANGUAGENAMELIST(LANGUAGENAMELIST value) {
            this.lANGUAGENAMELIST = value;
        }

        public String getPARENT() {
            return this.pARENT;
        }

        public void setPARENT(String value) {
            this.pARENT = value;
        }

        public String getGUID() {
            return this.gUID;
        }

        public void setGUID(String value) {
            this.gUID = value;
        }

        public String getBASEUNITS() {
            return this.bASEUNITS;
        }

        public void setBASEUNITS(String value) {
            this.bASEUNITS = value;
        }

        public String getGSTAPPLICABLE() {
            return this.gSTAPPLICABLE;
        }

        public void setGSTAPPLICABLE(String value) {
            this.gSTAPPLICABLE = value;
        }

        public String getID() {
            return this.ID;
        }

        public void setID(String value) {
            this.ID = value;
        }

        public GSTDETAILSLIST getGSTDETAILSLIST() {
            return this.gSTDETAILSLIST;
        }

        public void setGSTDETAILSLIST(GSTDETAILSLIST value) {
            this.gSTDETAILSLIST = value;
        }


        @Override
        public String toString() {
            return this.lANGUAGENAMELIST.nAME; // What to display in the Spinner list.
        }


    }

}