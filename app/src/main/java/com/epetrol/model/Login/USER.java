package com.epetrol.model.Login;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.net.URL;
import java.util.List;

@Root(name="USER")
public class USER {

    @Element(name="PASSWORD", required=false)
    String pASSWORD;

    @Element(name="USERTYPE", required=false)
    String uSERTYPE;

    @Element(name="USERID", required=false)
    String uSERID;

    @Element(name="ACTIVEANDROIDLOGIN", required=false)
    String aCTIVEANDROIDLOGIN;

    @Element(name="USERNAME", required=false)
    String uSERNAME;

    @Element(name="ROLEID", required=false)
    String rOLEID;

    @Element(name="FLAG", required=false)
    String fLAG;

    @Element(name="MOBILENO", required=false)
    String mOBILENO;

    public String getPASSWORD() {return this.pASSWORD;}
    public void setPASSWORD(String value) {this.pASSWORD = value;}

    public String getUSERTYPE() {return this.uSERTYPE;}
    public void setUSERTYPE(String value) {this.uSERTYPE = value;}

    public String getUSERID() {return this.uSERID;}
    public void setUSERID(String value) {this.uSERID = value;}

    public String getACTIVEANDROIDLOGIN() {return this.aCTIVEANDROIDLOGIN;}
    public void setACTIVEANDROIDLOGIN(String value) {this.aCTIVEANDROIDLOGIN = value;}

    public String getUSERNAME() {return this.uSERNAME;}
    public void setUSERNAME(String value) {this.uSERNAME = value;}

    public String getROLEID() {return this.rOLEID;}
    public void setROLEID(String value) {this.rOLEID = value;}

    public String getFLAG() {return this.fLAG;}
    public void setFLAG(String value) {this.fLAG = value;}

    public String getMOBILENO() {return this.mOBILENO;}
    public void setMOBILENO(String value) {this.mOBILENO = value;}

}