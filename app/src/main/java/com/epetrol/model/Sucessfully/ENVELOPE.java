package com.epetrol.model.Sucessfully;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.net.URL;
import java.util.List;

@Root(name="ENVELOPE")
public class ENVELOPE {

    @Element(name="COLLECTION", required=false)
    COLLECTION cOLLECTION;

 /*   @Text(required=false)
    String textValue;*/

    public COLLECTION getCOLLECTION() {return this.cOLLECTION;}
    public void setCOLLECTION(COLLECTION value) {this.cOLLECTION = value;}

  /*  public String getTextValue() {return this.textValue;}
    public void setTextValue(String value) {this.textValue = value;}
*/
    public static class COLLECTION {

        @Element(name="RECEIPT", required=false)
        RECEIPT rECEIPT;

        public RECEIPT getRECEIPT() {return this.rECEIPT;}
        public void setRECEIPT(RECEIPT value) {this.rECEIPT = value;}

    }

    public static class RECEIPT {

        @Element(name="SUFFIXNUMBER", required=false)
        String sUFFIXNUMBER;

        @Element(name="LEDGERNAME", required=false)
        String lEDGERNAME;

        @Element(name="REMARKS", required=false)
        String rEMARKS;

        @Element(name="RECEIPTTYPE", required=false)
        String rECEIPTTYPE;

        @Element(name="CHEQUEDD", required=false)
        String cHEQUEDD;

        @Element(name="VOUCHERNUMBER", required=false)
        String vOUCHERNUMBER;

        @Element(name="COMPGUID", required=false)
        String cOMPGUID;

        @Element(name="CUSTOMERID", required=false)
        String cUSTOMERID;

        @Element(name="VOUCHERDATE", required=false)
        String vOUCHERDATE;

        @Element(name="RECEIPTID", required=false)
        String rECEIPTID;

        @Element(name="AMOUNT", required=false)
        String aMOUNT;

        @Element(name="CUSTOMERGUID", required=false)
        String cUSTOMERGUID;

        @Element(name="CHEQUEIMAGE", required=false)
        String cHEQUEIMAGE;

        @Element(name="PREFIXNUMBER", required=false)
        String pREFIXNUMBER;

        @Element(name="CUSTOMERNAME", required=false)
        String cUSTOMERNAME;

        @Element(name="CREATIONBY", required=false)
        String cREATIONBY;

        public String getSUFFIXNUMBER() {return this.sUFFIXNUMBER;}
        public void setSUFFIXNUMBER(String value) {this.sUFFIXNUMBER = value;}

        public String getLEDGERNAME() {return this.lEDGERNAME;}
        public void setLEDGERNAME(String value) {this.lEDGERNAME = value;}

        public String getREMARKS() {return this.rEMARKS;}
        public void setREMARKS(String value) {this.rEMARKS = value;}

        public String getRECEIPTTYPE() {return this.rECEIPTTYPE;}
        public void setRECEIPTTYPE(String value) {this.rECEIPTTYPE = value;}

        public String getCHEQUEDD() {return this.cHEQUEDD;}
        public void setCHEQUEDD(String value) {this.cHEQUEDD = value;}

        public String getVOUCHERNUMBER() {return this.vOUCHERNUMBER;}
        public void setVOUCHERNUMBER(String value) {this.vOUCHERNUMBER = value;}

        public String getCOMPGUID() {return this.cOMPGUID;}
        public void setCOMPGUID(String value) {this.cOMPGUID = value;}

        public String getCUSTOMERID() {return this.cUSTOMERID;}
        public void setCUSTOMERID(String value) {this.cUSTOMERID = value;}

        public String getVOUCHERDATE() {return this.vOUCHERDATE;}
        public void setVOUCHERDATE(String value) {this.vOUCHERDATE = value;}

        public String getRECEIPTID() {return this.rECEIPTID;}
        public void setRECEIPTID(String value) {this.rECEIPTID = value;}

        public String getAMOUNT() {return this.aMOUNT;}
        public void setAMOUNT(String value) {this.aMOUNT = value;}

        public String getCUSTOMERGUID() {return this.cUSTOMERGUID;}
        public void setCUSTOMERGUID(String value) {this.cUSTOMERGUID = value;}

        public String getCHEQUEIMAGE() {return this.cHEQUEIMAGE;}
        public void setCHEQUEIMAGE(String value) {this.cHEQUEIMAGE = value;}

        public String getPREFIXNUMBER() {return this.pREFIXNUMBER;}
        public void setPREFIXNUMBER(String value) {this.pREFIXNUMBER = value;}

        public String getCUSTOMERNAME() {return this.cUSTOMERNAME;}
        public void setCUSTOMERNAME(String value) {this.cUSTOMERNAME = value;}

        public String getCREATIONBY() {return this.cREATIONBY;}
        public void setCREATIONBY(String value) {this.cREATIONBY = value;}

    }

}