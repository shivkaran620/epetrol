package com.epetrol.model.CompanyDetails;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.net.URL;
import java.util.List;

@Root(name="ENVELOPE")
public class ENVELOPE {

   /* @Text(required=false)
    String textValue;*/

    @Element(name="BODY", required=false)
    BODY bODY;

  /*  public String getTextValue() {return this.textValue;}
    public void setTextValue(String value) {this.textValue = value;}
*/
    public BODY getBODY() {return this.bODY;}
    public void setBODY(BODY value) {this.bODY = value;}

    public static class COLLECTION {

        @Element(name="COMPANY", required=false)
        COMPANY cOMPANY;

        public COMPANY getCOMPANY() {return this.cOMPANY;}
        public void setCOMPANY(COMPANY value) {this.cOMPANY = value;}

    }

    public static class CMPINFO {

        @Element(name="COMPANY", required=false)
        String cOMPANY;

        public String getCOMPANY() {return this.cOMPANY;}
        public void setCOMPANY(String value) {this.cOMPANY = value;}

    }

    public static class DESC {

        @Element(name="CMPINFO", required=false)
        CMPINFO cMPINFO;

        public CMPINFO getCMPINFO() {return this.cMPINFO;}
        public void setCMPINFO(CMPINFO value) {this.cMPINFO = value;}

    }

    public static class COMPANY {

        @Element(name="PO_SUFFIXNUMBER", required=false)
        String pOSUFFIXNUMBER;

        @Element(name="RECEIPT_SUFFIXNUMBER", required=false)
        String rECEIPTSUFFIXNUMBER;

        @Element(name="PHONENUMBER", required=false)
        String pHONENUMBER;

        @Element(name="_ADDRESS5", required=false)
        String ADDRESS5;

        @Element(name="GUID", required=false)
        String gUID;

        @Element(name="COMPANYNUMBER", required=false)
        String cOMPANYNUMBER;

        @Element(name="EMAIL", required=false)
        String eMAIL;

        @Element(name="INCOMETAXNUMBER", required=false)
        String iNCOMETAXNUMBER;

        @Element(name="SALES_SUFFIXNUMBER", required=false)
        String sALESSUFFIXNUMBER;

        @Element(name="NAME", required=false)
        String nAME;

        @Element(name="STATENAME", required=false)
        String sTATENAME;

        @Element(name="ALTERID", required=false)
        String aLTERID;

        @Element(name="PO_PREFIXNUMBER", required=false)
        String pOPREFIXNUMBER;

        @Element(name="PAYMENT_PREFIXNUMBER", required=false)
        String pAYMENTPREFIXNUMBER;

        @Element(name="RECEIPT_PREFIXNUMBER", required=false)
        String rECEIPTPREFIXNUMBER;

        @Element(name="_ADDRESS4", required=false)
        String ADDRESS4;

        @Element(name="_ADDRESS3", required=false)
        String ADDRESS3;

        @Element(name="_ADDRESS2", required=false)
        String ADDRESS2;

        @Element(name="_ADDRESS1", required=false)
        String ADDRESS1;

        @Element(name="PAYMENT_SUFFIXNUMBER", required=false)
        String pAYMENTSUFFIXNUMBER;

        @Element(name="COUNTRYNAME", required=false)
        String cOUNTRYNAME;

        @Element(name="SALES_PREFIXNUMBER", required=false)
        String sALESPREFIXNUMBER;

        @Element(name="WEBSITE", required=false)
        String wEBSITE;

        public String getPOSUFFIXNUMBER() {return this.pOSUFFIXNUMBER;}
        public void setPOSUFFIXNUMBER(String value) {this.pOSUFFIXNUMBER = value;}

        public String getRECEIPTSUFFIXNUMBER() {return this.rECEIPTSUFFIXNUMBER;}
        public void setRECEIPTSUFFIXNUMBER(String value) {this.rECEIPTSUFFIXNUMBER = value;}

        public String getPHONENUMBER() {return this.pHONENUMBER;}
        public void setPHONENUMBER(String value) {this.pHONENUMBER = value;}

        public String getADDRESS5() {return this.ADDRESS5;}
        public void setADDRESS5(String value) {this.ADDRESS5 = value;}

        public String getGUID() {return this.gUID;}
        public void setGUID(String value) {this.gUID = value;}

        public String getCOMPANYNUMBER() {return this.cOMPANYNUMBER;}
        public void setCOMPANYNUMBER(String value) {this.cOMPANYNUMBER = value;}

        public String getEMAIL() {return this.eMAIL;}
        public void setEMAIL(String value) {this.eMAIL = value;}

        public String getINCOMETAXNUMBER() {return this.iNCOMETAXNUMBER;}
        public void setINCOMETAXNUMBER(String value) {this.iNCOMETAXNUMBER = value;}

        public String getSALESSUFFIXNUMBER() {return this.sALESSUFFIXNUMBER;}
        public void setSALESSUFFIXNUMBER(String value) {this.sALESSUFFIXNUMBER = value;}

        public String getNAME() {return this.nAME;}
        public void setNAME(String value) {this.nAME = value;}

        public String getSTATENAME() {return this.sTATENAME;}
        public void setSTATENAME(String value) {this.sTATENAME = value;}

        public String getALTERID() {return this.aLTERID;}
        public void setALTERID(String value) {this.aLTERID = value;}

        public String getPOPREFIXNUMBER() {return this.pOPREFIXNUMBER;}
        public void setPOPREFIXNUMBER(String value) {this.pOPREFIXNUMBER = value;}

        public String getPAYMENTPREFIXNUMBER() {return this.pAYMENTPREFIXNUMBER;}
        public void setPAYMENTPREFIXNUMBER(String value) {this.pAYMENTPREFIXNUMBER = value;}

        public String getRECEIPTPREFIXNUMBER() {return this.rECEIPTPREFIXNUMBER;}
        public void setRECEIPTPREFIXNUMBER(String value) {this.rECEIPTPREFIXNUMBER = value;}

        public String getADDRESS4() {return this.ADDRESS4;}
        public void setADDRESS4(String value) {this.ADDRESS4 = value;}

        public String getADDRESS3() {return this.ADDRESS3;}
        public void setADDRESS3(String value) {this.ADDRESS3 = value;}

        public String getADDRESS2() {return this.ADDRESS2;}
        public void setADDRESS2(String value) {this.ADDRESS2 = value;}

        public String getADDRESS1() {return this.ADDRESS1;}
        public void setADDRESS1(String value) {this.ADDRESS1 = value;}

        public String getPAYMENTSUFFIXNUMBER() {return this.pAYMENTSUFFIXNUMBER;}
        public void setPAYMENTSUFFIXNUMBER(String value) {this.pAYMENTSUFFIXNUMBER = value;}

        public String getCOUNTRYNAME() {return this.cOUNTRYNAME;}
        public void setCOUNTRYNAME(String value) {this.cOUNTRYNAME = value;}

        public String getSALESPREFIXNUMBER() {return this.sALESPREFIXNUMBER;}
        public void setSALESPREFIXNUMBER(String value) {this.sALESPREFIXNUMBER = value;}

        public String getWEBSITE() {return this.wEBSITE;}
        public void setWEBSITE(String value) {this.wEBSITE = value;}

    }

    public static class DATA {

        @Element(name="COLLECTION", required=false)
        COLLECTION cOLLECTION;

        public COLLECTION getCOLLECTION() {return this.cOLLECTION;}
        public void setCOLLECTION(COLLECTION value) {this.cOLLECTION = value;}

    }

    public static class BODY {

        @Element(name="DATA", required=false)
        DATA dATA;

        @Element(name="DESC", required=false)
        DESC dESC;

        public DATA getDATA() {return this.dATA;}
        public void setDATA(DATA value) {this.dATA = value;}

        public DESC getDESC() {return this.dESC;}
        public void setDESC(DESC value) {this.dESC = value;}

    }

}