package com.epetrol.model.Company;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.net.URL;
import java.util.List;

@Root(name="ENVELOPE")
public class ENVELOPE {

    @ElementList(name="COLLECTION", required=false)
    List<COMPANYRIGHTS> cOLLECTION;

  /*  @Text(required=false)
    String textValue;*/

    public List<COMPANYRIGHTS> getCOLLECTION() {return this.cOLLECTION;}
    public void setCOLLECTION(List<COMPANYRIGHTS> value) {this.cOLLECTION = value;}

   /* public String getTextValue() {return this.textValue;}
    public void setTextValue(String value) {this.textValue = value;}*/

    public static class COMPANYRIGHTS {

        @Element(name="COMPMOBILE", required=false)
        String cOMPMOBILE;

        @Element(name="RIGHTVIEW", required=false)
        String rIGHTVIEW;

        @Element(name="COMPGUID", required=false)
        String cOMPGUID;

        @Element(name="RIGHTNO", required=false)
        String rIGHTNO;

        @Element(name="RIGHTNAME", required=false)
        String rIGHTNAME;

        @Element(name="ROLEID", required=false)
        String rOLEID;

        public String getCOMPMOBILE() {return this.cOMPMOBILE;}
        public void setCOMPMOBILE(String value) {this.cOMPMOBILE = value;}

        public String getRIGHTVIEW() {return this.rIGHTVIEW;}
        public void setRIGHTVIEW(String value) {this.rIGHTVIEW = value;}

        public String getCOMPGUID() {return this.cOMPGUID;}
        public void setCOMPGUID(String value) {this.cOMPGUID = value;}

        public String getRIGHTNO() {return this.rIGHTNO;}
        public void setRIGHTNO(String value) {this.rIGHTNO = value;}

        public String getRIGHTNAME() {return this.rIGHTNAME;}
        public void setRIGHTNAME(String value) {this.rIGHTNAME = value;}

        public String getROLEID() {return this.rOLEID;}
        public void setROLEID(String value) {this.rOLEID = value;}

    }

}