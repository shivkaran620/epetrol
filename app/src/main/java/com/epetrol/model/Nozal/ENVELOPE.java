package com.epetrol.model.Nozal;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.net.URL;
import java.util.List;

@Root(name="ENVELOPE")
public class ENVELOPE {

    @ElementList(name="COLLECTION", required=false)
    List<NOZEL> cOLLECTION;

  /*  @Text(required=false)
    String textValue;*/

    public List<NOZEL> getCOLLECTION() {return this.cOLLECTION;}
    public void setCOLLECTION(List<NOZEL> value) {this.cOLLECTION = value;}

  /*  public String getTextValue() {return this.textValue;}
    public void setTextValue(String value) {this.textValue = value;}
*/
    public static class NOZEL {

        @Element(name="COMPGUID", required=false)
        String cOMPGUID;

        @Element(name="NOZELID", required=false)
        String nOZELID;

        @Element(name="NOZELNAME", required=false)
        String nOZELNAME;

        @Element(name="ITEMGUID", required=false)
        String iTEMGUID;

        @Element(name="ITEMCOMPGUID", required=false)
        String iTEMCOMPGUID;

        @Element(name="OPENINGREADING", required=false)
        String oPENINGREADING;

        @Element(name="REMARKS", required=false)
        String rEMARKS;

        @Element(name="ITEMID", required=false)
        String iTEMID;

        @Element(name="CLOSINGREADING", required=false)
        String cLOSINGREADING;

        @Element(name="ITEMNAME", required=false)
        String iTEMNAME;


        @Element(name="RIGHTVIEW", required=false)
        String RIGHTVIEW;
        @Element(name="ROLEID", required=false)
        String ROLEID;



      public String getRIGHTVIEW() {
          return RIGHTVIEW;
      }

      public void setRIGHTVIEW(String RIGHTVIEW) {
          this.RIGHTVIEW = RIGHTVIEW;
      }

      public String getROLEID() {
          return ROLEID;
      }

      public void setROLEID(String ROLEID) {
          this.ROLEID = ROLEID;
      }

      public String getCOMPGUID() {return this.cOMPGUID;}
        public void setCOMPGUID(String value) {this.cOMPGUID = value;}

        public String getNOZELID() {return this.nOZELID;}
        public void setNOZELID(String value) {this.nOZELID = value;}

        public String getNOZELNAME() {return this.nOZELNAME;}
        public void setNOZELNAME(String value) {this.nOZELNAME = value;}

        public String getITEMGUID() {return this.iTEMGUID;}
        public void setITEMGUID(String value) {this.iTEMGUID = value;}

        public String getITEMCOMPGUID() {return this.iTEMCOMPGUID;}
        public void setITEMCOMPGUID(String value) {this.iTEMCOMPGUID = value;}

        public String getOPENINGREADING() {return this.oPENINGREADING;}
        public void setOPENINGREADING(String value) {this.oPENINGREADING = value;}

        public String getREMARKS() {return this.rEMARKS;}
        public void setREMARKS(String value) {this.rEMARKS = value;}

        public String getITEMID() {return this.iTEMID;}
        public void setITEMID(String value) {this.iTEMID = value;}

        public String getCLOSINGREADING() {return this.cLOSINGREADING;}
        public void setCLOSINGREADING(String value) {this.cLOSINGREADING = value;}

        public String getITEMNAME() {return this.iTEMNAME;}
        public void setITEMNAME(String value) {this.iTEMNAME = value;}

      @Override
      public String toString() {
          return this.nOZELNAME; // What to display in the Spinner list.
      }

    }

}