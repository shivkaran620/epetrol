package com.epetrol.retrofit

import androidx.databinding.library.BuildConfig
import com.epetrol.model.Company.ENVELOPE
import com.epetrol.model.Login.USER
import com.google.gson.GsonBuilder
import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.HttpsURLConnection

interface ApiService {


    companion object {

        var BASE_URL = "http://apipacificpetroleum.popularinfosys.com/api/"

        fun create(): ApiService {

            val hostnameVerifier =
                HostnameVerifier { hostname, session ->
                    val hv: HostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier()
                    hv.verify("localhost", session)
                }


            val client = OkHttpClient.Builder()
                .connectTimeout(2000, TimeUnit.SECONDS)
                .readTimeout(2000, TimeUnit.SECONDS)
                .hostnameVerifier(hostnameVerifier)
                .addInterceptor(object : Interceptor {
                    override fun intercept(chain: Interceptor.Chain): Response {
                        val newRequest = chain.request().newBuilder()
//                            .addHeader("Authorization", "Bearer $token")
                            .build()
                        return chain.proceed(newRequest)
                    }

                }).build()


            val okHttpBuilder = OkHttpClient.Builder()
            okHttpBuilder.addInterceptor(HttpLoggingInterceptor().apply {
                level =
                    if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            })

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .client(client)
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }

    /*  @FormUrlEncoded
      @POST("user_register")
      fun user_register(
          @Field("full_name") strFullName: String,
          @Field("email") strEmail: String,
          @Field("country_code") strCountryCode: String?,
          @Field("mobile") strMobile: String,
          @Field("password") strPasseord: String,
          @Field("device_id") deviceID: String,
          @Field("device_type") deviceType: String,
          @Field("fcm_token") token: String
      ): Single<GetResponseRegisterModel>*/

    @Headers("Content-Type: application/xml")
    @POST("role/company_right_list")
    fun getcompanylist(@Body body: RequestBody): Single<ENVELOPE>

    @Headers("Content-Type: application/xml")
    @POST("user/check_password")
    fun login(@Body toString: RequestBody): Single<USER>

    @Headers("Content-Type: application/xml")
    @POST("customer/list")
    fun customerlist(@Body body: RequestBody): Single<com.epetrol.model.CustomerList.ENVELOPE>

    @Headers("Content-Type: application/xml")
    @POST("company/list")
    fun CompanyDetails(@Body body: RequestBody): Single<com.epetrol.model.CompanyDetails.ENVELOPE>


    @Headers("Content-Type: application/xml")
    @POST("payment/add")
    fun paymentadd(@Body body: RequestBody): Single<com.epetrol.model.Sucessfully.Payment.ENVELOPE>

    @Headers("Content-Type: application/xml")
    @POST("receipt/add")
    fun Receiptsadd(@Body body: RequestBody): Single<com.epetrol.model.Sucessfully.ENVELOPE>

    @Headers("Content-Type: application/xml")
    @POST("nozel/list")
    fun nozellist(@Body body: RequestBody): Single<com.epetrol.model.Nozal.ENVELOPE>

    @Headers("Content-Type: application/xml")
    @POST("item/list")
    fun itemlist(@Body body: RequestBody): Single<com.epetrol.model.Item.ENVELOPE>


}