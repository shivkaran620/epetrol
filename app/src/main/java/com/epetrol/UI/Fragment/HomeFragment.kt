package com.epetrol.UI.Fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.epetrol.Helper.Constants
import com.epetrol.R
import com.epetrol.UI.Activity.AddSale.Step1PaymentType
import com.epetrol.UI.Activity.OutStaning.OutStandingActivity
import com.epetrol.UI.Activity.ReadingRegister.ReadingRegisterActivity
import com.epetrol.UI.Activity.Receipts.ReceiptsActivity
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.header_layout.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : BasicFragment(),View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        tvTitle.setText(resources.getString(R.string.home))
        img_back.isVisible = false

        tvuserName.setText(resources.getString(R.string.Welcome)+" "+prefs.getCompanyModel().name)


        card_add_sales.setOnClickListener(this)
        card_Recipts.setOnClickListener(this)
        card_Payment.setOnClickListener(this)
        card_out_standing.setOnClickListener(this)
        card_reading.setOnClickListener(this)



    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(v: View?) {
        when(v){
            card_add_sales->{
                startActivity(Intent(activity,Step1PaymentType::class.java))
            }
            card_Recipts->{
                startActivity(Intent(activity,ReceiptsActivity::class.java)
                    .putExtra(Constants.Receipt,Constants.Receipt))
            }
            card_Payment->{
                startActivity(Intent(activity,ReceiptsActivity::class.java)
                    .putExtra(Constants.Receipt,Constants.Payment))
            }
            card_out_standing->{
                startActivity(Intent(activity, OutStandingActivity::class.java))
            }
            card_reading->{
                startActivity(Intent(activity, ReadingRegisterActivity::class.java))
            }
        }
    }
}