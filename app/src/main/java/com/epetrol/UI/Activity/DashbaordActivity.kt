package com.epetrol.UI.Activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.epetrol.Helper.Constants
import com.epetrol.R
import com.epetrol.UI.Fragment.HomeFragment
import com.epetrol.viewmodel.CompanyViewModel
import com.geteztruck.customer.Helper.Utils
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_dashbaord.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import org.json.XML

class DashbaordActivity : BaseActivity() {

    lateinit var companyViewModel:CompanyViewModel
    var compguid = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashbaord)

        companyViewModel = ViewModelProvider(this).get(CompanyViewModel::class.java)


        if (intent.getStringExtra(Constants.CompanyGUID) != null){
            compguid = intent.getStringExtra(Constants.CompanyGUID).toString()
        }

        var jsonObject  = JSONObject()

        var json1 = JSONObject()
        json1.put("COMPGUID",compguid)
        json1.put("PAGEID",1)

        jsonObject.put("PARAM",json1)

        var xml = XML.toString(jsonObject)

        val body = xml.toRequestBody("application/xml; charset=utf-8".toMediaTypeOrNull())


        companyViewModel.CompanyDetails(body)

        getResponseData()





        nav_view.setOnNavigationItemSelectedListener(
            object : BottomNavigationView.OnNavigationItemSelectedListener {
                override fun onNavigationItemSelected(item: MenuItem): Boolean {
                    when (item.getItemId()) {

                        R.id.navigation_home -> {

                            loadFragment(HomeFragment())

                            return true
                        }
                        R.id.navigation_company -> {
                            startActivity(Intent(this@DashbaordActivity,CompanyActivity::class.java))

                            return true
                        }
                        R.id.navigation_logout -> {
                            startActivity(Intent(this@DashbaordActivity,LoginActivity::class.java))
                            finishAffinity()
                            return true
                        }


                    }
                    return true
                }
            })
    }

    private fun getResponseData() {

        companyViewModel.errorMsg.observe(this, Observer {
            Utils.showToast(this,it)
            System.out.println("compnay details "+it)
        })

        companyViewModel.responseCompanyDetails.observe(this, Observer {

            prefs.setCompanyModel(it.body.data.collection.company)
            loadFragment(HomeFragment())
        })
    }

    private fun loadFragment(homeFragment: HomeFragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.nav_host_fragment, homeFragment)
        transaction.commit()

    }
}