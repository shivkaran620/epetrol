package com.epetrol.UI.Activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.epetrol.R
import com.bumptech.glide.Glide
import com.epetrol.viewmodel.CompanyViewModel
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        /*   companyViewModel.getcompanylist()
   
           companyViewModel.responsePostlist.observe(this, Observer {
   
               Handler().postDelayed({
                   startActivity(Intent(this, LoginActivity::class.java))
                   finish()
               }, 3000)
           })
   
           companyViewModel.errorMsg.observe(this, Observer {
               System.out.println(it)
           })*/

        /*  Handler().postDelayed({
              startActivity(Intent(this, LoginActivity::class.java))
              finish()
          }, 3000)
  */
        Glide.with(this)
            .load(R.raw.logo)
            .into(img_logo);


        if (prefs.getLoginModel().userid != null) {

            Handler().postDelayed({
                startActivity(Intent(this, CompanyActivity::class.java))
                finish()
            }, 3000)

        } else {

            Handler().postDelayed({
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }, 3000)
        }


    }
}