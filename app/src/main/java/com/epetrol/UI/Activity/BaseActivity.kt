package com.epetrol.UI.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.epetrol.Helper.Prefs
import com.epetrol.R

open class BaseActivity : AppCompatActivity() {

    lateinit var prefs:Prefs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        prefs = Prefs(this)
    }
}