package com.epetrol.UI.Activity.Receipts

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.epetrol.Helper.Constants
import com.epetrol.R
import com.epetrol.UI.Activity.BaseActivity
import com.epetrol.UI.Adapter.LegerListAdapter
import com.epetrol.model.CustomerList.ENVELOPE
import com.epetrol.viewmodel.PaymentViewModel
import com.geteztruck.customer.Helper.Utils
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_receipts.*
import kotlinx.android.synthetic.main.bottomview_design_layout.*
import kotlinx.android.synthetic.main.header_layout.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import org.json.XML
import java.sql.Struct
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ReceiptsActivity : BaseActivity(), View.OnClickListener {

    var Type = ""
    lateinit var paymentViewModel: PaymentViewModel
    var page = 0
    var listofuser = ArrayList<ENVELOPE.LEDGER>()
    var listofuserLEDGERlist = ArrayList<ENVELOPE.LEDGER>()
    var usernamelist = ArrayList<Struct>()
    lateinit var legerListAdapter: LegerListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receipts)

        paymentViewModel = ViewModelProvider(this).get(PaymentViewModel::class.java)

        if (intent.getStringExtra(Constants.Receipt) != null) {
            Type = intent.getStringExtra(Constants.Receipt).toString()

        }


        if (Type.equals(Constants.Receipt)) {
            edt_supplier_name.setHint(resources.getString(R.string.please_enter_customer_name))
            tcvSuplierName.setText(resources.getString(R.string.customer_name))
            GetcustomerLEDGER(Constants.SundryDebtors)
            tvTitle.setText(resources.getString(R.string.Recipts))

        } else {
            GetcustomerLEDGER(Constants.SundryCreditors)
            tvTitle.setText(resources.getString(R.string.Payment))
        }


        getResponseData()


        val sdf = SimpleDateFormat("dd MMM yyyy")
        val currentDate = sdf.format(Date())

        edt_Date.setText(currentDate)

        edt_Date.setOnClickListener {
            Utils.setDatePickerDialog(this, edt_Date)
        }

        Getcustomerlist(Constants.CashInHand)

        llBank.setOnClickListener(this)
        ll_cash.setOnClickListener(this)
        tvNext.setOnClickListener(this)
        img_back.setOnClickListener(this)
        sp_ledger_type.setOnClickListener(this)
    }

    private fun GetcustomerLEDGER(sundryDebtors: String) {
        var jsonObject = JSONObject()

        var json1 = JSONObject()

        json1.put("COMPGUID", prefs.getCompanyModel().guid)
        json1.put("PRIMARYGROUPNAME", sundryDebtors)
        json1.put("PAGEID", page)

        jsonObject.put("PARAM", json1)

        var xml = XML.toString(jsonObject)

        val body = xml.toRequestBody("application/xml; charset=utf-8".toMediaTypeOrNull())

        paymentViewModel.customerlistLEDGERName(body)


    }

    private fun Getcustomerlist(cashInHand: String) {

        var jsonObject = JSONObject()

        var json1 = JSONObject()

        json1.put("COMPGUID", prefs.getCompanyModel().guid)
        json1.put("PRIMARYGROUPNAME", cashInHand)
        json1.put("PAGEID", page)

        jsonObject.put("PARAM", json1)

        var xml = XML.toString(jsonObject)

        val body = xml.toRequestBody("application/xml; charset=utf-8".toMediaTypeOrNull())

        paymentViewModel.customerlist(body)


    }

    private fun getResponseData() {

        paymentViewModel.isLoading.observe(this, androidx.lifecycle.Observer {
            if (it) {
                Utils.showProgress(this)
            } else {
                Utils.hideProgress()
            }
        })

        paymentViewModel.errorMsg.observe(this, androidx.lifecycle.Observer {
            Utils.showToast(this, it)
        })


        paymentViewModel.responseSucessully.observe(this, androidx.lifecycle.Observer {
            if (Type.equals(Constants.Receipt)) {

                prefs.setReciptData(it)

                startActivity(
                    Intent(
                        this,
                        ReceiptsSlipActivity::class.java
                    ).putExtra(Constants.Type, Constants.Receipt)
                )
                finish()

            }
        })

        paymentViewModel.responsePaymentSucessully.observe(this, androidx.lifecycle.Observer {
            prefs.setPaymentData(it)
            startActivity(
                Intent(this, ReceiptsSlipActivity::class.java)
                    .putExtra(Constants.Type, Constants.Payment)
            )
            finish()
        })


        paymentViewModel.responseCustomerlist.observe(this, androidx.lifecycle.Observer {

            listofuser.addAll(it.body.data.collection)


            /*   val userAdapter: ArrayAdapter<*> =
                   ArrayAdapter<ENVELOPE.LEDGER>(this, com.epetrol.R.layout.spinner, listofuser)
               sp_ledger_type.adapter = userAdapter
   */

        })
        paymentViewModel.responseLEDGERlist.observe(this, androidx.lifecycle.Observer {

            listofuserLEDGERlist.addAll(it.body.data.collection)

            System.out.println("customer name " + listofuserLEDGERlist.get(0).languagenamE_LIST.name)

            val adapter =
                ArrayAdapter(this, android.R.layout.simple_list_item_1, listofuserLEDGERlist)
            edt_supplier_name.setAdapter(adapter)


        })


    }

    override fun onClick(v: View?) {
        when (v) {
            img_back -> {
                onBackPressed()
            }
            llBank -> {
                setUI()
                llBank.setBackgroundDrawable(resources.getDrawable(R.drawable.btn_blue))
                tvBank.setTextColor(getResources().getColor(R.color.white));
                ll_check_number.visibility = View.VISIBLE
                listofuser.clear()
                Getcustomerlist(Constants.BankAccounts)

            }
            ll_cash -> {
                setUI()
                listofuser.clear()
                ll_cash.setBackgroundDrawable(resources.getDrawable(R.drawable.btn_blue))
                tvCash.setTextColor(getResources().getColor(R.color.white));
                ll_check_number.visibility = View.GONE
                Getcustomerlist(Constants.CashInHand)
            }
            sp_ledger_type -> {
                BottomSheetView()
            }
            tvNext -> {

                var str_date =
                    Utils.formatDate(edt_Date.text.toString().trim(), "dd MMM yyyy", "yyyy/MM/dd")
                var ledger_type =
                    sp_ledger_type.text.toString() /*sp_ledger_type.selectedItem.toString()*/

                var CUSTOMERID = ""
                var flag = false
                for (i in 0 until listofuserLEDGERlist.size) {
                    var name = listofuserLEDGERlist.get(i).languagenamE_LIST.name
                    if (name.equals(edt_supplier_name.text.toString())) {
                        CUSTOMERID = listofuserLEDGERlist.get(i).id
                        flag = true
                        break
                    }

                }

                if (!flag) {
                    edt_supplier_name.error =
                        resources.getString(R.string.please_enter_customer_name)
                } else {
                    var supplier_name = edt_supplier_name.text.toString()
                    var amount = edt_amount.text.toString()
                    var chequeorDDNumber = edt_ChequeorDDNumber.text.toString()
                    var remark = edt_remark.text.toString()
                    var RECEIPTTYPE = ""

                    if (Type.equals(Constants.Receipt)) {
                        RECEIPTTYPE = "0"
                    } else {
                        RECEIPTTYPE = "1"
                    }

                    when (!Utils.isEmptyEditText(
                        edt_amount,
                        resources.getString(R.string.Pleaseentervalidamount)
                    )) {
                        true
                        -> {
                            var jsonObject = JSONObject()
                            var jsonObject_COLLECTION = JSONObject()
                            var jsonObject_ENVELOPE = JSONObject()

                            var json1 = JSONObject()
                            if (Type.equals(Constants.Receipt)) {
                                json1.put(
                                    "PREFIXNUMBER",
                                    prefs.getCompanyModel().receiptprefixnumber
                                )
                                json1.put(
                                    "SUFFIXNUMBER",
                                    prefs.getCompanyModel().receiptsuffixnumber
                                )
                            } else {
                                json1.put(
                                    "PREFIXNUMBER",
                                    prefs.getCompanyModel().paymentprefixnumber
                                )
                                json1.put(
                                    "SUFFIXNUMBER",
                                    prefs.getCompanyModel().paymentsuffixnumber
                                )

                            }
                            json1.put("VOUCHERDATE", str_date)
                            json1.put("CUSTOMERID", CUSTOMERID)
                            json1.put("CUSTOMERNAME", supplier_name)
                            json1.put("LEDGERNAME", ledger_type)
                            json1.put("AMOUNT", amount)
                            json1.put("CHEQUEDD", chequeorDDNumber)
                            json1.put("REMARKS", remark)
                            json1.put("RECEIPTTYPE", RECEIPTTYPE)
                            json1.put("COMPGUID", prefs.getCompanyModel().guid)
                            json1.put("CREATIONBY", prefs.getLoginModel().userid)

                            if (Type.equals(Constants.Receipt)) {
                                jsonObject.put("RECEIPT", json1)
                            } else {
                                jsonObject.put("PAYMENT", json1)

                            }
                            jsonObject_COLLECTION.put("COLLECTION", jsonObject)
                            jsonObject_ENVELOPE.put("ENVELOPE", jsonObject_COLLECTION)

                            var xml = XML.toString(jsonObject_ENVELOPE)

                            val body =
                                xml.toRequestBody("application/xml; charset=utf-8".toMediaTypeOrNull())

                            System.out.println("customer name " + jsonObject_ENVELOPE.toString())



                            if (Type.equals(Constants.Receipt)) {
                                paymentViewModel.Receiptsadd(body)

                            } else {
                                paymentViewModel.paymentadd(body)
                            }

                        }
                    }
                }
            }
        }
    }

    private fun BottomSheetView() {

        var mBottomSheetDialog = BottomSheetDialog(this,R.style.DialogStyle)
        mBottomSheetDialog.setContentView(R.layout.bottomview_design_layout)


        mBottomSheetDialog.edt_seach.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                FilterName(s.toString())


            }
        })

        legerListAdapter = LegerListAdapter(this, listofuser) {
            sp_ledger_type.setText(it.languagenamE_LIST.name)
            mBottomSheetDialog.dismiss()
        }
        mBottomSheetDialog.rv_findlist.adapter = legerListAdapter
        mBottomSheetDialog.rv_findlist.layoutManager = LinearLayoutManager(this)

        mBottomSheetDialog.show()
    }

    private fun FilterName(toString: String) {

        val data = listofuser.filter {
            it.languagenamE_LIST.name.toString().toLowerCase().contains(toString.toLowerCase())
        }
        legerListAdapter.updatedata(data)
    }

    private fun setUI() {

        llBank.setBackgroundDrawable(resources.getDrawable(R.drawable.boder_blue))
        ll_cash.setBackgroundDrawable(resources.getDrawable(R.drawable.boder_blue))
        tvCash.setTextColor(getResources().getColor(R.color.black));
        tvBank.setTextColor(getResources().getColor(R.color.black));


    }
}