package com.epetrol.UI.Activity.OutStaning

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.epetrol.R
import com.epetrol.UI.Activity.BaseActivity
import com.epetrol.UI.Adapter.OutStandingAdapter
import kotlinx.android.synthetic.main.activity_out_standing.*
import kotlinx.android.synthetic.main.header_layout.*

class OutStandingActivity : BaseActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_out_standing)

        tvTitle.setText(resources.getString(R.string.outstanding))

        var listof = ArrayList<String>()
        listof.add("10000")
        listof.add("10000")
        listof.add("10000")
        listof.add("10000")

        var outStandingAdapter = OutStandingAdapter(this,listof){

        }
        rvoutStanding.adapter = outStandingAdapter
        rvoutStanding.layoutManager = LinearLayoutManager(this)

        img_back.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v) {
            img_back -> {
                onBackPressed()
            }
        }
    }
}