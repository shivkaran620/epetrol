package com.epetrol.UI.Activity.AddSale

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.epetrol.R
import com.epetrol.UI.Activity.BaseActivity
import kotlinx.android.synthetic.main.header_layout.*

class Invocie_Activity : BaseActivity(),View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invocie)

        tvTitle.setText(resources.getString(R.string.GenrateInvocie))

        img_back.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        when(v){
            img_back->{
                onBackPressed()
            }
        }
    }
}