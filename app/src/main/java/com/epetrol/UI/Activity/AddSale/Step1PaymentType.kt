package com.epetrol.UI.Activity.AddSale

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.epetrol.Helper.Constants
import com.epetrol.R
import com.epetrol.UI.Activity.BaseActivity
import kotlinx.android.synthetic.main.activity_step1_payment_type.*
import kotlinx.android.synthetic.main.header_layout.*

class Step1PaymentType : BaseActivity(),View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_step1_payment_type)



        card_debit.setOnClickListener(this)
        card_cash.setOnClickListener(this)
        img_back.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v){
            img_back->{
                onBackPressed()
            }
            card_debit->{
               startActivity(Intent(this,Step2CustomerLis::class.java).putExtra(Constants.Type,Constants.debit))
            }
            card_cash->{
                startActivity(Intent(this,Step2CustomerLis::class.java).putExtra(Constants.Type,Constants.cash))
            }
        }
    }


}