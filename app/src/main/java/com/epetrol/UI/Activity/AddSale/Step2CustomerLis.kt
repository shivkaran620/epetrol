package com.epetrol.UI.Activity.AddSale

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.NonNull
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.epetrol.R
import com.epetrol.UI.Activity.BaseActivity
import com.epetrol.UI.Adapter.CustomerListAdapter
import com.epetrol.viewmodel.CompanyViewModel
import com.geteztruck.customer.Helper.Utils
import kotlinx.android.synthetic.main.activity_step2_customer_lis.*
import kotlinx.android.synthetic.main.header_layout.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import org.json.XML
import androidx.recyclerview.widget.RecyclerView
import com.epetrol.Helper.Constants
import com.epetrol.model.CustomerList.ENVELOPE


class Step2CustomerLis : BaseActivity(), View.OnClickListener {

    lateinit var companyViewModel: CompanyViewModel
    lateinit var customerListAdapter: CustomerListAdapter
    var page = 0
    var type = ""
    var PRIMARYGROUPNAME = ""

    var listofuser = ArrayList<ENVELOPE.LEDGER>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_step2_customer_lis)

        companyViewModel = ViewModelProvider(this).get(CompanyViewModel::class.java)

        tvTitle.setText(resources.getString(R.string.customerlist))
        img_back.isVisible = true

        if (intent.getStringExtra(Constants.Type) != null) {
            type = intent.getStringExtra(Constants.Type).toString()
        }

        if (type.equals(Constants.debit)){
            PRIMARYGROUPNAME = Constants.SundryDebtors
        }else{
            PRIMARYGROUPNAME = Constants.CashInHand
        }


        var jsonObject = JSONObject()

        var json1 = JSONObject()
        json1.put("COMPGUID", prefs.getCompanyModel().guid)
        json1.put("PRIMARYGROUPNAME", PRIMARYGROUPNAME)
        json1.put("PAGEID", page)

        jsonObject.put("PARAM", json1)

        var xml = XML.toString(jsonObject)

        val body = xml.toRequestBody("application/xml; charset=utf-8".toMediaTypeOrNull())

        companyViewModel.customerlist(body)


        GetResponseList()



        img_back.setOnClickListener(this)
    }

    private fun GetResponseList() {

       /* companyViewModel.isLoading.observe(this, Observer {
            if (it) {
                Utils.showProgress(this)
            } else {
                Utils.hideProgress()
            }
        })*/

        companyViewModel.errorMsg.observe(this, Observer {
            System.out.println(it)
            Utils.showToast(this, it)
        })

        companyViewModel.responseCustomerlist.observe(this, Observer {

            listofuser.addAll(it.body.data.collection)

            if (page == 0) {

                customerListAdapter = CustomerListAdapter(this, listofuser) {

                    var salemodel = prefs.getSaleModel()
                    salemodel.CUSTOMERID = it.id
                    salemodel.CUSTOMERNAME = it.languagenamE_LIST.name

                    prefs.setSaleModel(salemodel)

                    startActivity(Intent(this, Step3AddSalesDetails::class.java))


                }
                rvCompanylist.adapter = customerListAdapter
                rvCompanylist.layoutManager = LinearLayoutManager(this)

                rvCompanylist.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrolled(@NonNull recyclerView: RecyclerView, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)
                        if (!recyclerView.canScrollVertically(1)) { //1 for down
                            loadMore()
                        }
                    }
                })
            } else {
                customerListAdapter.updatedata(listofuser)
            }

        })

    }

    private fun loadMore() {

        page = page + 1

        var jsonObject = JSONObject()

        var json1 = JSONObject()
        json1.put("COMPGUID", prefs.getCompanyModel().guid)
        json1.put("PAGEID", page)

        jsonObject.put("PARAM", json1)

        var xml = XML.toString(jsonObject)

        val body = xml.toRequestBody("application/xml; charset=utf-8".toMediaTypeOrNull())

        companyViewModel.customerlist(body)

    }

    override fun onClick(v: View?) {
        when (v) {
            img_back -> {
                onBackPressed()
            }
        }
    }
}