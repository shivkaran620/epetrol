package com.epetrol.UI.Activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.epetrol.R
import com.epetrol.viewmodel.LoginViewModel
import com.geteztruck.customer.Helper.Utils
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import org.json.JSONObject
import org.json.XML
import okhttp3.RequestBody.Companion.toRequestBody


class LoginActivity : BaseActivity(), View.OnClickListener {

    lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        Glide.with(this)
            .load(R.raw.logo)
            .fitCenter()
            .into(img_logo);


        GetresponseData()

        tvLogin.setOnClickListener(this)

    }

    private fun GetresponseData() {

        loginViewModel.isLoading.observe(this, Observer {
            if (it){
                Utils.showProgress(this)
            }else{
                Utils.hideProgress()
            }
        })

        loginViewModel.responseLogin.observe(this, Observer {
            if (it.userid != null){

                prefs.setLoginModel(it)

                System.out.println("data "+ it.userid)
                startActivity(Intent(this, CompanyActivity::class.java))
                finishAffinity()
            }
        })
        loginViewModel.errorMsg.observe(this, Observer {
            Utils.showToast(this,it)
            System.out.println("data "+ it)
        })
    }

    override fun onClick(v: View?) {
        when (v) {
            tvLogin -> {

                if (edt_Email.text.toString().trim().equals("")) {
                    edt_Email.error = resources.getString(R.string.pleaseenterusername)
                }  else {

                   /* var user = USER()
                    user.USERNAME = edt_Email.text.toString().trim()
                    user.PASSWORD = edt_password.text.toString().trim()

                    val builder = MultipartBody.Builder()
                    builder.setType(MultipartBody.FORM)

                    builder.addFormDataPart("USERNAME",edt_Email.text.toString().trim())
                    builder.addFormDataPart("PASSWORD",edt_password.text.toString().trim())
                    val requestBody = builder.build()
*/
                    var jsonObject  = JSONObject()

                    var json1 = JSONObject()
                    json1.put("USERNAME",edt_Email.text.toString().trim())
                    json1.put("PASSWORD",edt_password.text.toString().trim())

                    jsonObject.put("USER",json1)

                    var xml = XML.toString(jsonObject)

                    val body = xml.toRequestBody("application/xml; charset=utf-8".toMediaTypeOrNull())


                    loginViewModel.login(body)

                   /* startActivity(Intent(this, CompanyActivity::class.java))
                    finishAffinity()*/
                }
            }
        }
    }
}