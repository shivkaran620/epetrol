package com.epetrol.UI.Activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.epetrol.Helper.Constants
import com.epetrol.R
import com.epetrol.UI.Adapter.CompanyListAdapter
import com.epetrol.viewmodel.CompanyViewModel
import com.geteztruck.customer.Helper.Utils
import kotlinx.android.synthetic.main.activity_company.*
import kotlinx.android.synthetic.main.header_layout.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import org.json.XML

class CompanyActivity : BaseActivity(),View.OnClickListener {

    lateinit var companyViewModel: CompanyViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company)

        companyViewModel = ViewModelProvider(this).get(CompanyViewModel::class.java)


        tvTitle.setText(resources.getString(R.string.compnaylist))
        img_back.isVisible = true

        var jsonObject  = JSONObject()

        var json1 = JSONObject()
        json1.put("ID",prefs.getLoginModel().userid.toInt())
        json1.put("PAGEID",0)

        jsonObject.put("PARAM",json1)

        var xml = XML.toString(jsonObject)

        val body = xml.toRequestBody("application/xml; charset=utf-8".toMediaTypeOrNull())


        companyViewModel.getcompanylist(body)


        GetResponseData()

        img_back.setOnClickListener(this)
    }

    private fun GetResponseData() {

        companyViewModel.isLoading.observe(this, Observer {
            if (it){
                Utils.showProgress(this)
            }else{
                Utils.hideProgress()
            }
        })

        companyViewModel.errorMsg.observe(this, Observer {
            System.out.println(it)
            Utils.showToast(this,it)
        })

        companyViewModel.responsePostlist.observe(this, Observer {

            System.out.println("fata get "+ it.collection.size)

            var companyListAdapter = CompanyListAdapter(this, it.collection){

                prefs.setStringValue(Constants.ROLEID,it.roleid)
                startActivity(Intent(this,DashbaordActivity::class.java)
                    .putExtra(Constants.CompanyGUID,it.compguid))
                finishAffinity()
            }
            newConpnay.adapter = companyListAdapter
            newConpnay.layoutManager = LinearLayoutManager(this)
        })

    }

    override fun onClick(v: View?) {
        when(v){
            img_back->{
                onBackPressed()
            }
        }
    }
}