package com.epetrol.UI.Activity.Receipts

import android.os.Bundle
import android.view.View
import com.epetrol.Helper.Constants
import com.epetrol.R
import com.epetrol.UI.Activity.BaseActivity
import kotlinx.android.synthetic.main.activity_receipts_slip.*
import kotlinx.android.synthetic.main.header_layout.*

class ReceiptsSlipActivity : BaseActivity(),View.OnClickListener {

    var type = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receipts_slip)

        if (intent.getStringExtra(Constants.Type) != null){
            type = intent.getStringExtra(Constants.Type).toString()
        }


        if (type.equals(Constants.Receipt)) {

            tvReciptName.setText(prefs.getReciptData().collection.receipt.ledgername.toString())
            tvReciptID.setText(resources.getString(R.string.Reciptsno) + " " + prefs.getReciptData().collection.receipt.receiptid.toString())
            tvDate.setText(prefs.getReciptData().collection.receipt.voucherdate.toString())
            tvcustomer_name.setText(prefs.getReciptData().collection.receipt.customername.toString())
            tvPriceMessage.setText("We have recevied amount of ₹" + prefs.getReciptData().collection.receipt.amount.toString()+" Cash to "+prefs.getReciptData().collection.receipt.ledgername.toString())
            if (prefs.getReciptData().collection.receipt.remarks != null) {
                tvRemark_message.setText(prefs.getReciptData().collection.receipt.remarks.toString())
            }
        }else{
            tvReciptName.setText(prefs.getPaymentData().collection.payment.ledgername.toString())
            tvReciptID.setText(resources.getString(R.string.Paymentno) + " " + prefs.getPaymentData().collection.payment.paymentid.toString())
            tvDate.setText(prefs.getPaymentData().collection.payment.voucherdate.toString())
            tvcustomer_name.setText(prefs.getPaymentData().collection.payment.customername.toString())
            tvPriceMessage.setText("We have recevied amount of ₹" + prefs.getPaymentData().collection.payment.amount.toString()+" Cash to "+prefs.getPaymentData().collection.payment.ledgername.toString())

            if (prefs.getPaymentData().collection.payment.remarks != null) {
                tvRemark_message.setText(prefs.getPaymentData().collection.payment.remarks.toString())
            }
        }

        img_back.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v){
            img_back->{
                onBackPressed()
            }
        }
    }
}