package com.epetrol.UI.Activity.AddSale

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.epetrol.Helper.Constants
import com.epetrol.R
import com.epetrol.UI.Activity.BaseActivity
import com.epetrol.model.Item.ENVELOPE
import com.epetrol.viewmodel.SaleViewModel
import com.geteztruck.customer.Helper.Utils
import kotlinx.android.synthetic.main.activity_receipts.*
import kotlinx.android.synthetic.main.activity_step3_add_sales_details.*
import kotlinx.android.synthetic.main.activity_step3_add_sales_details.edt_Date
import kotlinx.android.synthetic.main.activity_step3_add_sales_details.tvNext
import kotlinx.android.synthetic.main.header_layout.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import org.json.XML


class Step3AddSalesDetails : BaseActivity(), View.OnClickListener {

    lateinit var saleViewModel: SaleViewModel

    var listofItem = ArrayList<ENVELOPE.STOCKITEM>()
    var listofnozel = ArrayList<com.epetrol.model.Nozal.ENVELOPE.NOZEL>()
    var listofnozel_update = ArrayList<com.epetrol.model.Nozal.ENVELOPE.NOZEL>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_step3_add_sales_details)

        saleViewModel = ViewModelProvider(this).get(SaleViewModel::class.java)

        tvTitle.setText(resources.getString(R.string.ItemDetails))

        tvcustomer_name.setText(resources.getString(R.string.CustomerName)+": "+prefs.getSaleModel().CUSTOMERNAME)

        var jsonObject = JSONObject()
        var jsonObject_noze = JSONObject()

        var json1 = JSONObject()
        json1.put("COMPGUID", prefs.getCompanyModel().guid)
        json1.put("PAGEID", -1)
        jsonObject.put("PARAM", json1)
        var xml = XML.toString(jsonObject)
        val body = xml.toRequestBody("application/xml; charset=utf-8".toMediaTypeOrNull())

        var json1_noze = JSONObject()
        json1_noze.put("ID", prefs.getStringValue(Constants.ROLEID))
        json1_noze.put("COMPGUID", prefs.getCompanyModel().guid)
        json1_noze.put("PAGEID", -1)
        jsonObject_noze.put("PARAM", json1_noze)
        var xml_noze = XML.toString(jsonObject_noze)
        val body_noze = xml_noze.toRequestBody("application/xml; charset=utf-8".toMediaTypeOrNull())

        saleViewModel.nozellist(body_noze)
        saleViewModel.itemlist(body)


        GetResponse()


        edt_Date.setOnClickListener {
            Utils.setDatePickerDialog(this, edt_Date)
        }

        img_back.setOnClickListener(this)
        tvNext.setOnClickListener(this)
    }

    private fun GetResponse() {

        saleViewModel.isLoading.observe(this, Observer {
            if (it) {
                Utils.showProgress(this)
            } else {
                Utils.hideProgress()
            }
        })

        saleViewModel.errorMsg.observe(this, Observer {
            Utils.showToast(this, it)
            System.out.println("error message " + it)
        })


        saleViewModel.responseNozellist.observe(this, Observer {
            listofnozel.addAll(it.collection)
            System.out.println("item list "+ listofnozel.size)


            /*  val userAdapter: ArrayAdapter<*> = ArrayAdapter<com.epetrol.model.Nozal.ENVELOPE.NOZEL>(this, R.layout.spinner, listofnozel)
              sp_nozel_name.adapter = userAdapter*/
        })

        saleViewModel.responseItemlist.observe(this, Observer {

            listofItem.addAll(it.body.data.collection)


            val userAdapter: ArrayAdapter<*> = ArrayAdapter<ENVELOPE.STOCKITEM>(this, R.layout.spinner, listofItem)
            sp_fuel_item.adapter = userAdapter

            sp_fuel_item?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {

                    UpdateNozellist(listofItem.get(position).id)

                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }


            }


        })

    }

    private fun UpdateNozellist(id: String?) {
        listofnozel_update.clear()
        for (i in 0 until listofnozel.size) {

            if (listofnozel.get(i).itemid.equals(id)) {
                listofnozel_update.add(listofnozel.get(i))
            }
        }

        System.out.println("item list listofnozel_update "+ listofnozel_update.size)


        val userAdapter: ArrayAdapter<*> = ArrayAdapter<com.epetrol.model.Nozal.ENVELOPE.NOZEL>(
            this,
            R.layout.spinner,
            listofnozel_update
        )
        sp_nozel_name.adapter = userAdapter

    }

    override fun onClick(v: View?) {
        when (v) {
            img_back -> {
                onBackPressed()
            }
            tvNext -> {
                startActivity(Intent(this, Invocie_Activity::class.java))
            }
        }
    }
}