package com.epetrol.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.epetrol.R
import com.epetrol.databinding.ListOfCompanyBinding
import com.epetrol.model.CustomerList.ENVELOPE
import java.util.ArrayList

class CustomerListAdapter(
    val context: Context,
    var listofcompnay: MutableList<ENVELOPE.LEDGER>,
    var function: (ENVELOPE.LEDGER) -> Unit
) :
    RecyclerView.Adapter<CustomerListAdapter.ViewHolder>() {
    private var currentPosition: Int = -1


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(p0.context)
        val binding: ListOfCompanyBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.list_of_company, p0, false)

        return ViewHolder(binding.root)
    }

    override fun getItemCount(): Int {
        return listofcompnay!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding!!.tvComnayname.setText(listofcompnay!!.get(position).languagenamE_LIST.name)
        holder.binding!!.tvComnaynumber.setText(listofcompnay!!.get(position).primarygroup.toString())

        holder.itemView.setOnClickListener {
            function(listofcompnay!!.get(position))
        }

    }

    fun updatedata(listofuser: ArrayList<ENVELOPE.LEDGER>) {

        listofcompnay = listofuser
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var binding: ListOfCompanyBinding? = null

        init {
            binding = DataBindingUtil.bind<ListOfCompanyBinding>(itemView)

        }
    }


}