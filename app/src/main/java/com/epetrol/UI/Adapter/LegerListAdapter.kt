package com.epetrol.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.epetrol.R
import com.epetrol.databinding.RowListOfLegerTypeBinding
import com.epetrol.model.CustomerList.ENVELOPE

class LegerListAdapter(
    val context: Context,
    var listofcompnay: ArrayList<ENVELOPE.LEDGER>,
    var function: (ENVELOPE.LEDGER) -> Unit
) :
    RecyclerView.Adapter<LegerListAdapter.ViewHolder>() {
    private var currentPosition: Int = -1


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(p0.context)
        val binding: RowListOfLegerTypeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_list_of_leger_type, p0, false)

        return ViewHolder(binding.root)
    }

    override fun getItemCount(): Int {
        return listofcompnay.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.binding!!.tvName.setText(listofcompnay.get(position).languagenamE_LIST.name)

        holder.itemView.setOnClickListener {
            function(listofcompnay.get(position))
        }

    }

    fun updatedata(data: List<ENVELOPE.LEDGER>) {

        listofcompnay = data as ArrayList<ENVELOPE.LEDGER>
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var binding: RowListOfLegerTypeBinding? = null

        init {
            binding = DataBindingUtil.bind<RowListOfLegerTypeBinding>(itemView)

        }
    }


}