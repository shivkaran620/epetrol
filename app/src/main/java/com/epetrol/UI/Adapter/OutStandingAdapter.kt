package com.epetrol.UI.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.epetrol.R
import com.epetrol.UI.Activity.DashbaordActivity
import com.epetrol.databinding.ListOfCompanyBinding
import com.epetrol.databinding.RowListOfOutStandingBinding

class OutStandingAdapter(
    val context: Context,
    var listofcompnay: ArrayList<String>,
    var function: (String) -> Unit
) :
    RecyclerView.Adapter<OutStandingAdapter.ViewHolder>() {
    private var currentPosition: Int = -1


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(p0.context)
        val binding: RowListOfOutStandingBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_list_of_out_standing, p0, false)

        return ViewHolder(binding.root)
    }

    override fun getItemCount(): Int {
        return listofcompnay.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.binding!!.Debit.setText(listofcompnay.get(position))

     /*   holder.itemView.setOnClickListener {
            function(listofcompnay.get(position))
        }*/

    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var binding: RowListOfOutStandingBinding? = null

        init {
            binding = DataBindingUtil.bind<RowListOfOutStandingBinding>(itemView)

        }
    }


}