package com.epetrol.Helper

object Constants {
    val DATA: String="DATA"
    val ROLEID: String="ROLEID"
    val CashInHand: String="Cash-In-Hand"
    val SundryDebtors: String="Sundry Debtors"
    val SundryCreditors: String="Sundry Creditors"
    val BankAccounts: String="Bank Accounts"
    val debit: String? = "debit"
    val cash: String? = "cash"
    val Type: String="Type"
    val Payment: String="Payment"
    val Receipt: String = "Receipt"
    val login_model: String = "LoginDetails"
    val CompanyDetails: String = "CompanyDetails"
    val CompanyGUID: String = "CompanyGUID"
    val SaleModel: String = "SaleModel"
    const val LOGINRESPONSEMODEL: String = "login_response_model"
    const val FCM_TOKEN: String = "FCM_TOKEN"
    const val ANDROID: String = "android"
    const val appLanguage: String = "appLanguage"
    const val Authorization: String = "Authorization"











}