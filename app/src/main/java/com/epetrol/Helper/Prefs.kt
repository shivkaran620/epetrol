package com.epetrol.Helper

import android.content.Context
import android.content.SharedPreferences
import com.epetrol.model.AddSaleModel.SaleModel
import com.epetrol.model.Company.ENVELOPE
import com.epetrol.model.Login.USER
import com.google.gson.Gson


class Prefs(context: Context) {

    val PREFS_FILENAME = context.packageName + ".prefs"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0);

    fun clear() {
        val editor = prefs.edit()
        editor.clear()
        editor.apply()
    }

    fun setStringValue(key: String, value: String?) {
        val editor = prefs.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getStringValue(key: String): String? {
        return prefs.getString(key, "")
    }


    fun setToken(context:Context,token: String){
        val prefs: SharedPreferences = context.getSharedPreferences("firebase",0)
        val editor = prefs.edit()
        editor.putString(Constants.Authorization,token)
        editor.apply()
    }

    fun getToken(context:Context):String{
        val prefs: SharedPreferences = context.getSharedPreferences("firebase",0)
        return prefs.getString(Constants.Authorization,"")!!
    }

    fun setLanuage(context:Context,token: String){
        val prefs: SharedPreferences = context.getSharedPreferences("lang",0)
        val editor = prefs.edit()
        editor.putString(Constants.Authorization,token)
        editor.apply()
    }

    fun getLanuage(context:Context):String{
        val prefs: SharedPreferences = context.getSharedPreferences("lang",0)
        return prefs.getString(Constants.Authorization,"")!!
    }

    fun setLoginModel(response: USER) {
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(response)
        editor.putString(Constants.login_model, json)
        editor.apply()

    }

    fun getLoginModel(): USER {
        val gson = Gson()
        val json = prefs.getString(Constants.login_model, "")
        if (json.isNullOrEmpty())
            return USER()
        else
            return gson.fromJson<USER>(json, USER::class.java)
    }

    fun setCompanyModel(response: com.epetrol.model.CompanyDetails.ENVELOPE.COMPANY) {
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(response)
        editor.putString(Constants.CompanyDetails, json)
        editor.apply()

    }

    fun getCompanyModel(): com.epetrol.model.CompanyDetails.ENVELOPE.COMPANY {
        val gson = Gson()
        val json = prefs.getString(Constants.CompanyDetails, "")
        if (json.isNullOrEmpty())
            return com.epetrol.model.CompanyDetails.ENVELOPE.COMPANY()
        else
            return gson.fromJson<com.epetrol.model.CompanyDetails.ENVELOPE.COMPANY>(json, com.epetrol.model.CompanyDetails.ENVELOPE.COMPANY::class.java)
    }



    fun getSaleModel(): SaleModel {
        val gson = Gson()
        val json = prefs.getString(Constants.SaleModel, "")
        if (json.isNullOrEmpty())
            return SaleModel()
        else
            return gson.fromJson<SaleModel>(json, SaleModel::class.java)

    }
    fun setSaleModel(response: SaleModel) {
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(response)
        editor.putString(Constants.SaleModel, json)
        editor.apply()

    }

    fun setReciptData(response: com.epetrol.model.Sucessfully.ENVELOPE?) {
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(response)
        editor.putString(Constants.Receipt, json)
        editor.apply()

    }

    fun getReciptData(): com.epetrol.model.Sucessfully.ENVELOPE {
        val gson = Gson()
        val json = prefs.getString(Constants.Receipt, "")
        if (json.isNullOrEmpty())
            return com.epetrol.model.Sucessfully.ENVELOPE()
        else
            return gson.fromJson<com.epetrol.model.Sucessfully.ENVELOPE>(json, com.epetrol.model.Sucessfully.ENVELOPE::class.java)

    }

    fun setPaymentData(response: com.epetrol.model.Sucessfully.Payment.ENVELOPE?) {
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(response)
        editor.putString(Constants.Payment, json)
        editor.apply()

    }
    fun getPaymentData(): com.epetrol.model.Sucessfully.Payment.ENVELOPE {
        val gson = Gson()
        val json = prefs.getString(Constants.Payment, "")
        if (json.isNullOrEmpty())
            return com.epetrol.model.Sucessfully.Payment.ENVELOPE()
        else
            return gson.fromJson<com.epetrol.model.Sucessfully.Payment.ENVELOPE>(json, com.epetrol.model.Sucessfully.Payment.ENVELOPE::class.java)

    }

}