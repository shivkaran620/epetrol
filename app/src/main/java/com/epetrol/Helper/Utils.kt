package com.geteztruck.customer.Helper


import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.*
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Handler
import android.text.format.DateUtils
import android.util.DisplayMetrics
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.epetrol.R
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


/**
 * Created by Admin on 19-01-2018.
 */

object Utils {

    private lateinit var dialog: Dialog

    fun showProgress(context: Context) {
        dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.progress_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    fun hideProgress() {
        dialog.dismiss()
    }

    fun showErrorDialog(
        context: Activity,
        message: String,
        buttonText: String,
        function: () -> Unit
    ) {
        val dialogBuilder = AlertDialog.Builder(context, R.style.TranslucentDialog)
        val inflater = context.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_error, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)
        val tv_message = dialogView.findViewById<TextView>(R.id.tv_message)
        val tv_button = dialogView.findViewById<TextView>(R.id.tv_button)

        tv_message.text = message
        tv_button.text = buttonText

        val alertDialog = dialogBuilder.create()
        tv_button.setOnClickListener {
            alertDialog.dismiss()
            function()
        }

        alertDialog.show()

    }

    fun showErrorDialogproduct(context: Activity, message: String, buttonText: String) {
        val dialogBuilder = AlertDialog.Builder(context, R.style.TranslucentDialog)
        val inflater = context.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_error, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)
        val tv_message = dialogView.findViewById<TextView>(R.id.tv_message)
        val tv_button = dialogView.findViewById<TextView>(R.id.tv_button)
        tv_button.visibility = View.VISIBLE

        tv_message.text = message
        tv_button.text = buttonText



        val alertDialog = dialogBuilder.create()
        tv_button.setOnClickListener {
            alertDialog.dismiss()
        }
        Handler().postDelayed({
            alertDialog.dismiss()
        }, 3000)

        alertDialog.show()

    }


    /*fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }*/

  /*  fun createMutilPart(file: File, key: String): MultipartBody.Part {
        val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
        return MultipartBody.Part.createFormData(key, file.name, requestFile)
    }

    fun createRequestBody(value: String?): RequestBody {
        return RequestBody.create("multipart/form-data".toMediaTypeOrNull(), value.toString())
    }*/

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    fun isEmptyEditText(edt: EditText, msg: String): Boolean {
        if (edt.text.toString().trim().isEmpty()) {
            edt.error = msg
            edt.requestFocus()
            return true
        }
        return false
    }


    fun isValidPhoneNumber(target: String?): Boolean {
        return if (target == null || target.length < 6 || target.length > 13) {
            false
        } else {
            Patterns.PHONE.matcher(target).matches()
            true
        }
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    fun isEmptyTextView(edt: TextView, msg: String): Boolean {
        if (edt.text.toString().isEmpty()) {
            edt.error = msg
            edt.requestFocus()
            return true
        }
        return false
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    fun isPasswordMatch(edt: EditText, editText: EditText, msg: String): Boolean {

        var str_pass = edt.text.toString()
        var str_pass2 = editText.text.toString()


        if (!str_pass.equals(str_pass2)) {
            editText.error = msg
            editText.requestFocus()
            return true
        }
        return false
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    fun isPhoneCorrect(edt: EditText): Boolean {
        if (edt.text.toString().length < 6) {
            edt.error = "Please enter correct pin code "
            edt.requestFocus()
            return true
        }
        return false
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    fun isEmailCorrect(context: Context,edt: EditText, message: String): Boolean {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        if (!edt.text.toString().trim { it <= ' ' }.matches(emailPattern.toRegex())) {
            edt.error = message
            edt.requestFocus()
            return true
        }
        return false
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    fun isPasswordCorrect(edt: EditText,message:String): Boolean {
//        val passwordPattern = "^([a-zA-Z+]+[0-9+]+[&@!#+]+)\$"
//        val passwordPattern = "^(?=.*\\d)(?=.*[a-zA-Z]).{4,8}\$"  working
        val passwordPattern = "^(?=.*[@$%&#_()=+?»«<>£§€{}\\[\\]-])(?=.*[A-Z])(?=.*[a-z])(?=.*\\d).*(?<=.{8,20})$"
        if (!edt.text.toString().trim { it <= ' ' }
                .matches(passwordPattern.toRegex()) || edt.text.toString().length < 8) {
            edt.error =message
            edt.requestFocus()
            return true
        }
        return false
    }


    fun formatDate(
        date: String, initDateFormat: String, endDateFormat: String
    ): String? {
        var parsedDate: String? = null
        val initDate: Date
        try {
            initDate = SimpleDateFormat(initDateFormat, Locale.getDefault()).parse(date)
            val formatter = SimpleDateFormat(endDateFormat, Locale.getDefault())
            parsedDate = formatter.format(initDate)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return parsedDate
    }

    fun formatDateItalic(
        date: String, initDateFormat: String, endDateFormat: String
    ): String? {
        var parsedDate: String? = null
        val initDate: Date
        try {
            initDate = SimpleDateFormat(initDateFormat, Locale.ITALIAN).parse(date)
            val formatter = SimpleDateFormat(endDateFormat, Locale.ITALIAN)
            parsedDate = formatter.format(initDate)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return parsedDate
    }

    fun getAddress(context: Context, lat: Double, lng: Double): String {
        val geocoder: Geocoder = Geocoder(context, Locale.getDefault())
        val addresses: List<Address>
        var address = ""
        try {
            addresses = geocoder.getFromLocation(
                lat,
                lng,
                1
            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses.isNotEmpty()) {
                address =
                    addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                val city = addresses[0].locality
                val state = addresses[0].adminArea
                val country = addresses[0].countryName
                val postalCode = addresses[0].postalCode
                val knownName = addresses[0].featureName
            }
            return address
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return address
    }


    fun getWidthOfScreen(mContext: Context): Int {
        val displayMetrics = DisplayMetrics()
        try {
            (mContext as Activity).windowManager
                .defaultDisplay
                .getMetrics(displayMetrics)
        } catch (e: Exception) {
            val windowManager = mContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowManager.defaultDisplay.getMetrics(displayMetrics)
        }

        return displayMetrics.widthPixels
    }

    fun calculateNoOfColumns(context: Context): Int {
        val displayMetrics = context.resources.displayMetrics
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        return (dpWidth / 180).toInt()
    }


    @SuppressLint("NewApi")
    fun setDatePickerDialog(context: Context, editText: EditText) {
            val dateFormatter = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())

            val newCalendar = Calendar.getInstance()
            val dateDialog = DatePickerDialog(
                context,
                R.style.DialogTheme,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val newDate = Calendar.getInstance()
                    newDate.set(year, monthOfYear, dayOfMonth)

                    editText.setText(dateFormatter.format(newDate.time))
                },
                newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH)
            )
            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val inputDate: Date
            try {
                inputDate = dateFormat.parse(dateFormat.format(Date()))
                dateDialog.datePicker.minDate = inputDate.time

                dateDialog.show()
            } catch (e: ParseException) {
                e.printStackTrace()
            }

    }



    fun getDistance(startLat: Double, startLong: Double, endLat: Double, endLong: Double): Double {
        val startPoint = Location("locationA")
        startPoint.latitude = startLat
        startPoint.longitude = startLong

        val endPoint = Location("locationA")
        endPoint.latitude = endLat
        endPoint.longitude = endLong

        val distance = startPoint.distanceTo(endPoint) / 1000
        val number3digits: Double = String.format("%.3f", distance).toDouble()
        val number2digits: Double = String.format("%.2f", number3digits).toDouble()

        return String.format("%.1f", number2digits).toDouble()
    }

    fun isPasswordnotmatched(edt: EditText): Boolean {

        val specialCharPattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
        val upperCasePattern = Pattern.compile("[A-Z ]")
        val lowerCasePattern = Pattern.compile("[a-z ]")
        val digitCasePattern = Pattern.compile("[0-9 ]")

        val password = edt.getText().toString()

        if (password.length < 8 || password.length > 32) {

//            edt.error = "Password validate 8 digits"
            edt.error =
                "Password validate 8 digits, contains upper,lowerCase,and a special Character"
            edt.requestFocus()
            return true
        }

        if (!specialCharPattern.matcher(password).find()) {

//            edt.error = "Password validate  a special Character"
            edt.error =
                "Password validate 8 digits, contains upper,lowerCase,and a special Character"
            edt.requestFocus()
            return true
        }

        if (!upperCasePattern.matcher(password).find()) {
            edt.error =
                "Password validate 8 digits, contains upper,lowerCase,and a special Character"
//            edt.error = "Password validate  contains upperCase"
            edt.requestFocus()
            return true
        }

        if (!lowerCasePattern.matcher(password).find()) {

            edt.error =
                "Password validate 8 digits, contains upper,lowerCase,and a special Character"
//            edt.error = "Password validate lowerCase"
            edt.requestFocus()
            return true
        }

        if (!digitCasePattern.matcher(password).find()) {
            //            input_password.setError(getString(R.string.password_digit_error));
            //            input_password.requestFocus();

            edt.error =
                "Password validate 8 digits, contains upper,lowerCase,and a special Character"
            edt.requestFocus()
            return true
        }

//        edt.setError(null)

        return false
    }


    @SuppressLint("NewApi")
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    @SuppressLint("WrongConstant")
    fun showToast(context: Context, text: String?) {
//        val toast = CustomToast.makeText(context, text, Toast.LENGTH_LONG)
//        toast.duration = 7000
//        toast.show()
//        showFlashBar(context,text)

        Toast.makeText(context,text,Toast.LENGTH_LONG).show()
    }

   /* fun createRequestBody(value: String?): RequestBody {
        return  value.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
    }


    fun createMutilPart(file: File, key: String): MultipartBody.Part {
        val requestFile = file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
        return MultipartBody.Part.createFormData(key, file.name, requestFile)
    }

    fun createVideoMutilPart(file: File, key: String): MultipartBody.Part {
        val requestFile = file.asRequestBody("video/mp4".toMediaTypeOrNull())
        return MultipartBody.Part.createFormData(
            key,
            URLEncoder.encode(file.name, "utf-8"),
            requestFile
        )
    }*/

    fun setTimePickerDialog(context: Context, edtDeliverytime: TextView?) {

        val c = Calendar.getInstance()
        var mHour = c[Calendar.HOUR_OF_DAY]
        var mMinute = c[Calendar.MINUTE]

        // Launch Time Picker Dialog

        // Launch Time Picker Dialog
        val timePickerDialog = TimePickerDialog(
            context,
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                mHour = hourOfDay
                mMinute = minute
                edtDeliverytime!!.setText(hourOfDay.toString() + ":" + minute)
            }, mHour, mMinute, false
        )
        timePickerDialog.show()
    }
    fun findToday(value: Long): String {
        return when {
            DateUtils.isToday(value) -> "Today"
            DateUtils.isToday(value + DateUtils.DAY_IN_MILLIS) -> "Yesterday"
            else -> {
                val formatter = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
                formatter.format(Date(value))
            }
        }
    }


}
