package com.epetrol.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.epetrol.model.Sucessfully.ENVELOPE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody

class PaymentViewModel(application: Application):BaseViewModel(application) {

    val responseSucessully = MutableLiveData<ENVELOPE>()
    val responsePaymentSucessully = MutableLiveData<com.epetrol.model.Sucessfully.Payment.ENVELOPE>()
    val responseCustomerlist = MutableLiveData<com.epetrol.model.CustomerList.ENVELOPE>()
    val responseLEDGERlist = MutableLiveData<com.epetrol.model.CustomerList.ENVELOPE>()


    fun paymentadd(body: RequestBody) {
        isLoading.value = true
        disposable = apiService
            .paymentadd(
                body
            ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                responsePaymentSucessully.value = result
                isLoading.value = false
            }, { error ->
                isLoading.value = false
                val c = error.cause
                errorMsg.value = error.message
            })
    }
    fun Receiptsadd(body: RequestBody) {
        isLoading.value = true
        disposable = apiService
            .Receiptsadd(
                body
            ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                responseSucessully.value = result
                isLoading.value = false
            }, { error ->
                isLoading.value = false
                val c = error.cause
                errorMsg.value = error.message
            })
    }

    fun customerlist(body: RequestBody) {
        isLoading.value = true
        disposable = apiService
            .customerlist(
                body
            ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                responseCustomerlist.value = result
                isLoading.value = false
            }, { error ->
                isLoading.value = false
                val c = error.cause
                errorMsg.value = error.message
            })
    }

    fun customerlistLEDGERName(body: RequestBody) {
        disposable = apiService
            .customerlist(
                body
            ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                responseLEDGERlist.value = result
            }, { error ->
                val c = error.cause
                errorMsg.value = error.message
            })
    }

}