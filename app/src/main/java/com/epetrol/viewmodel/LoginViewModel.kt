package com.epetrol.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.epetrol.model.Login.USER
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody

class LoginViewModel(application: Application):BaseViewModel(application) {

    val responseLogin = MutableLiveData<USER>()


    fun login(user: RequestBody) {
        isLoading.value = true
        disposable = apiService
            .login(
                user
            ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                responseLogin.value = result
                isLoading.value = false
            }, { error ->
                isLoading.value = false
                val c = error.cause
                errorMsg.value = error.message
            })
    }


}