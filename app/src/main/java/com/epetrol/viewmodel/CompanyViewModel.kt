package com.epetrol.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.epetrol.model.Company.ENVELOPE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody

class CompanyViewModel(application: Application):BaseViewModel(application) {


    val responsePostlist = MutableLiveData<ENVELOPE>()
    val responseCustomerlist = MutableLiveData<com.epetrol.model.CustomerList.ENVELOPE>()
    val responseCompanyDetails = MutableLiveData<com.epetrol.model.CompanyDetails.ENVELOPE>()


    fun getcompanylist(body: RequestBody) {

        disposable = apiService
            .getcompanylist(
                body
            ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                responsePostlist.value = result
                //isLoading.value = false
            }, { error ->
                //isLoading.value = false
                val c = error.cause
                errorMsg.value = error.message
            })
    }

    fun customerlist(body: RequestBody) {
        isLoading.value = true
        disposable = apiService
            .customerlist(
                body
            ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                responseCustomerlist.value = result
                isLoading.value = false
            }, { error ->
                isLoading.value = false
                val c = error.cause
                errorMsg.value = error.message
            })
    }

    fun CompanyDetails(body: RequestBody) {
        isLoading.value = true
        disposable = apiService
            .CompanyDetails(
                body
            ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                responseCompanyDetails.value = result
                isLoading.value = false
            }, { error ->
                isLoading.value = false
                val c = error.cause
                errorMsg.value = error.message
            })

    }

}