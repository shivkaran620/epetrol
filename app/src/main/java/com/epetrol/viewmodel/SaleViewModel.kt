package com.epetrol.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.epetrol.model.Nozal.ENVELOPE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody

class SaleViewModel(application: Application):BaseViewModel(application) {

    var responseNozellist = MutableLiveData<ENVELOPE>()
    var responseItemlist = MutableLiveData<com.epetrol.model.Item.ENVELOPE>()


    fun nozellist(body: RequestBody) {
        isLoading.value = true
        disposable = apiService
            .nozellist(
                body
            ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                responseNozellist.value = result
                isLoading.value = false
            }, { error ->
                isLoading.value = false
                val c = error.cause
                errorMsg.value = error.message
            })
    }

    fun itemlist(body: RequestBody) {
        disposable = apiService
            .itemlist(
                body
            ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                responseItemlist.value = result
            }, { error ->
                val c = error.cause
                errorMsg.value = error.message
            })

    }


}