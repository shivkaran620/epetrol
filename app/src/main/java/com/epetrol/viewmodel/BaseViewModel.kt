package com.epetrol.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.epetrol.retrofit.ApiService
import io.reactivex.disposables.Disposable

open class BaseViewModel(application: Application) : AndroidViewModel(application) {
    val isLoading = MutableLiveData<Boolean>()
    var disposable: Disposable? = null
    var errorMsg = MutableLiveData<String>()

    val apiService by lazy {
//        ApiService.create()
        ApiService.create()

    }

    override fun onCleared() {
        disposable?.dispose()
    }
}